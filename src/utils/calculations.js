// For weather functionality
export function convertToCelsius(temp) {
  return Math.round(temp - 273.15);
}

// To be refactored, main svg generating code.
export function generatePieChartPaths(total, elements) {
  let pathsArray = [];
  let cumulativePercentage = 0;
  for (let el in elements) {
    let path = "";
    let percentage = elements[el] / total;
    let x = Math.cos(2 * Math.PI * cumulativePercentage);
    let y = Math.sin(2 * Math.PI * cumulativePercentage);
    path += `M ${x} ${y} `;
    cumulativePercentage += percentage;
    x = Math.cos(2 * Math.PI * cumulativePercentage);
    y = Math.sin(2 * Math.PI * cumulativePercentage);
    path += `A 1 1 0 ${percentage > 0.5 ? "1" : "0"} 1 ${x} ${y} L 0 0`;

    pathsArray.push({
      path: path,
      fill: `rgb(${Math.random() * 255},${Math.random() * 255},${Math.random() *
        255})`,
      title: el
    });
  }
  return pathsArray;
}
