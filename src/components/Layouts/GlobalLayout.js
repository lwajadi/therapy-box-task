import React from "react";
import styled from "styled-components";

import "./global.css";
import background from "../../images/background.jpg";

const BackgroundImage = styled.div`
    background-image: url("${background}");
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: -10;
`;

const GlobalLayout = ({ children }) => {
  return (
    <>
      <BackgroundImage />
      {children}
    </>
  );
};

export default GlobalLayout;
