import React, { useState, useContext } from "react";
import axios from "axios";
import jwt from "jsonwebtoken";

import history from "../../utils/history";

import { AuthContext } from "../../context/AuthProvider";

import {
  LoginButton,
  LoginFields,
  LoginInput,
  SignupForm
} from "../UI/Elements";

/*
    **TODO**

    Signup form layout with controlled inputs + password validation. Uses Context to manage create user requests and to login on success.
*/

const adapter = axios.create({
  baseURL: "https://immense-cliffs-89443.herokuapp.com/",
  headers: {
    Authorization: `Bearer {token}`
  }
});

const Signup = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const Auth = useContext(AuthContext);

  const handleUsernameChange = event => {
    setUsername(event.target.value);
  };

  const handleEmailChange = event => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  };

  const handleConfirmPasswordChange = event => {
    setConfirmPassword(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();

    adapter
      .post("/api/user/register", {
        username: username,
        email: email,
        password: password
      })
      .then(response => {
        const token = response.headers.authorization.slice(7);
        Auth.setToken(token);
        const user = jwt.verify(token, process.env.REACT_APP_TOKEN_SECRET);
        // Flawed, better implementation to store username in JWT
        Auth.setUsername(username);
        Auth.setUser(user.id);
        history.push("/");
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <div>
      <SignupForm>
        <LoginFields>
          <LoginInput
            value={username}
            onChange={handleUsernameChange}
            placeholder="Username"
          />
          <LoginInput
            value={email}
            onChange={handleEmailChange}
            placeholder="Email"
          />
        </LoginFields>
        <LoginFields>
          <LoginInput
            value={password}
            onChange={handlePasswordChange}
            placeholder="Password"
            type="password"
          />
          <LoginInput
            value={confirmPassword}
            onChange={handleConfirmPasswordChange}
            placeholder="Confirm Password"
            type="password"
          />
        </LoginFields>
        <LoginButton onClick={handleSubmit}>Sign Up</LoginButton>
      </SignupForm>
    </div>
  );
};

export default Signup;
