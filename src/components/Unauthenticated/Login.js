import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import jwt from "jsonwebtoken";

import history from "../../utils/history";

import { AuthContext } from "../../context/AuthProvider";

import {
  LoginForm,
  LoginButton,
  LoginSpan,
  LoginText,
  LoginInput,
  LoginFields
} from "../UI/Elements";

/*
    **TODO**

    Login form layout with controlled inputs. Uses Context to login.
*/

const adapter = axios.create({
  baseURL: "https://immense-cliffs-89443.herokuapp.com/"
});

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const Auth = useContext(AuthContext);

  const handleUsernameChange = event => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();

    adapter
      .post("/api/user/login", {
        username: username,
        password: password
      })
      .then(response => {
        const token = response.headers.authorization.slice(7);
        Auth.setToken(token);
        const user = jwt.verify(token, process.env.REACT_APP_TOKEN_SECRET);
        // Flawed, better implementation to store username in JWT
        Auth.setUsername(username);
        Auth.setUser(user.id);
        history.push("/");
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  return (
    <>
      <LoginForm>
        <LoginFields>
          <LoginInput
            value={username}
            onChange={handleUsernameChange}
            placeholder="Username"
          />
          <LoginInput
            value={password}
            onChange={handlePasswordChange}
            placeholder="Password"
            type="password"
          />
        </LoginFields>
        <LoginButton onClick={handleSubmit}>Login</LoginButton>
      </LoginForm>
      <LoginFields>
        <LoginText>
          New to the hackathon?{" "}
          <Link to={"/signup"} style={{ textDecoration: "none" }}>
            <LoginSpan>Sign Up</LoginSpan>
          </Link>
        </LoginText>
      </LoginFields>
    </>
  );
};

export default Login;
