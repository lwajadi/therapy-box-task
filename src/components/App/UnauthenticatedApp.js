import React from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import styled from "styled-components";

import Login from "../Unauthenticated/Login";
import Signup from "../Unauthenticated/Signup";

/*
    **TODO**
    
    Login and sign-up logic and routing goes here.
    This component only appears if no user is currently logged in.
*/

const UnauthenticatedApp = () => {
  return (
    <Div>
      <H1>Hackathon</H1>
      <Switch>
        <Route path="/(|login)/" component={Login} />
        <Route path="/signup" component={Signup} />
        <Redirect from="*" to="/" />
      </Switch>
    </Div>
  );
};

const H1 = styled.h1`
  font-size: 3.5em;
  color: white;
  text-align: center;
`;

const Div = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

export default UnauthenticatedApp;
