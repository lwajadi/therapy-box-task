import React, { useState, useEffect } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import axios from "axios";

import Dashboard from "../Authenticated/Dashboard";
import News from "../Authenticated/News";
import Sport from "../Authenticated/Sport";
import Photos from "../Authenticated/Photos";
import Tasks from "../Authenticated/Tasks";

import { convertToCelsius } from "../../utils/calculations";

/*
    App functionality for logged in users goes here.

    Not displayed if a user has not already logged in.
*/

// Digests the football CSV received
const processCSV = csv => {
  let csvLines = csv.split(/\r\n|\n/);
  let teams = [];
  let results = [];
  csvLines.map(line => {
    let lineArray = line.split(",");
    teams.push([lineArray.slice(2, 4)]);
    results.push(lineArray.slice(6, 7));
  });
  // Removes the header fields for simplicity
  teams = teams.slice(1);
  results = results.slice(1);

  return { selectedTeam: null, pairs: teams, results: results };
};

const AuthenticatedApp = () => {
  // Hooks used to store each seperate widget's state
  const [weatherState, setWeatherState] = useState({});
  const [newsState, setNewsState] = useState({});
  const [sportState, setSportState] = useState({});
  const [photosState, setPhotosState] = useState({});
  const [tasksState, setTasksState] = useState({});
  const [clothesState, setClothesState] = useState({});

  /* 
      Weather Functionality
      
      State: {
        icon: Stores main weather state (i.e "Cloudy", "Clear")
        temperature: Stores temperature in Celsius
        location: Stores the current location used for the weather data
      }

  */
  const weatherFunctionality = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        let latitude = position.coords.latitude;
        let longitude = position.coords.longitude;
        let appId = process.env.REACT_APP_WEATHER_SECRET;
        axios
          .get(
            `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${appId}`
          )
          .then(({ data }) => {
            setWeatherState({
              icon: data.weather[0].main,
              temperature: convertToCelsius(data.main.temp),
              location: data.name
            });
          });
      });
    }
  };

  /* 
      Clothes Functionality
      
      State: {
        total: Total count of clothing items
        items: Object containing tally of each clothing type
      }
      
  */
  const clothesFunctionality = () => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/https://therapy-box.co.uk/hackathon/clothing-api.php?username=swapnil"
      )
      .then(({ data }) => {
        let clothes = data.payload.reduce((acc, current) => {
          if (typeof acc[current.clothe] == "undefined") {
            acc[current.clothe] = 1;
          } else {
            acc[current.clothe] += 1;
          }
          return acc;
        }, {});
        setClothesState({ total: data.payload.length, items: clothes });
      });
  };

  /* 
      News Functionality
      
      State: {
        headline: Corresponds to title in the BBC feed
        text: Summary of the news text
      }
      
  */
  const newsFunctionality = () => {
    axios
      .get(
        "https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.bbci.co.uk%2Fnews%2Frss.xml"
      )
      .then(({ data }) => {
        setNewsState({
          headline: data.items[0].title,
          text: data.items[0].description,
          imageUrl: data.imageUrl
        });
      });
  };

  /* 
      Sports Functionality
      
      State: {
        selectedTeam: Current user-selected team
        wonAgainst: Array of teams the selected team has beaten
        pairs: Array of pairs of teams who played each other. [0] : Home team, [1] : Away team
        results: Result of pair matchup. "H" = pairs[i][0] won, "A" = pairs[i][1] won, "D" = neither team won
      }
      
  */
  const sportFunctionality = () => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/http://www.football-data.co.uk/mmz4281/1718/I1.csv"
      )
      .then(({ data }) => {
        let csvData = processCSV(data);
        setSportState(csvData);
      });
  };

  const selectTeam = team => {
    let teamsBeaten = [];
    for (let i = 0; i < sportState.pairs.length; i++) {
      if (sportState.pairs[i][0][0] == team && sportState.results[i] == "H") {
        if (!teamsBeaten.includes(sportState.pairs[i][0][1])) {
          teamsBeaten.push(sportState.pairs[i][0][1]);
        }
      }
      if (sportState.pairs[i][0][1] == team && sportState.results[i] == "A") {
        if (!teamsBeaten.includes(sportState.pairs[i][0][0])) {
          teamsBeaten.push(sportState.pairs[i][0][0]);
        }
      }
    }
    if (teamsBeaten.length > 0) {
      setSportState(prevState => ({
        ...prevState,
        selectedTeam: team,
        wonAgainst: teamsBeaten
      }));
    } else {
      alert("This team has not played in this division");
    }
  };

  // ComponentDidMount = Load the data for the widgets and store it in state
  useEffect(() => {
    clothesFunctionality();
    newsFunctionality();
    sportFunctionality();
    weatherFunctionality();
  }, []);

  return (
    <div>
      <Switch>
        <Route
          exact
          path="/"
          render={routeProps => (
            <Dashboard
              {...routeProps}
              weatherState={weatherState}
              clothesState={clothesState}
              sportState={sportState}
              newsState={newsState}
            />
          )}
        />
        <Route
          path="/news"
          render={routeProps => <News {...routeProps} newsState={newsState} />}
        />
        <Route
          path="/sport"
          render={routeProps => (
            <Sport
              {...routeProps}
              sportState={sportState}
              selectTeam={selectTeam}
            />
          )}
        />
        <Route
          path="/photos"
          render={routeProps => (
            <Photos {...routeProps} photosState={photosState} />
          )}
        />
        <Route
          path="/tasks"
          render={routeProps => (
            <Tasks {...routeProps} tasksState={tasksState} />
          )}
        />
        <Redirect from="*" to="/" />
      </Switch>
    </div>
  );
};

export default AuthenticatedApp;
