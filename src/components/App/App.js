import React, { Suspense, useContext } from "react";
import { Router, Switch, Route } from "react-router-dom";
import history from "../../utils/history";

//import AuthenticatedApp from "./AuthenticatedApp";
import UnauthenticatedApp from "./UnauthenticatedApp";
import GlobalLayout from "../Layouts/GlobalLayout";

import { AuthContext } from "../../context/AuthProvider";

const AuthenticatedApp = React.lazy(() => import("./AuthenticatedApp"));

/*
    Main app logic. Uses AuthContext and UserContext to determine whether to show the user the Authenticated App, which contains all the logic and routes related to the main app. Otherwise UnauthenticatedApp handles the routes and logic related to logging in and signing up to the application.
*/

function App() {
  // Used to decide whether to display the authenticated routes or not. (Dummy to be replaced by functional login system)
  // const user = null;

  const Auth = useContext(AuthContext);
  console.log(Auth.getToken());

  return (
    <GlobalLayout>
      <Router history={history}>
        {Auth.getToken() ? (
          <Suspense fallback={<div>Loading...</div>}>
            <AuthenticatedApp />
          </Suspense>
        ) : (
          <UnauthenticatedApp />
        )}
      </Router>
    </GlobalLayout>
  );
}

export default App;
