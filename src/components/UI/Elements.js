import React from "react";
import styled from "styled-components";

import widgetContainer from "../../images/container.png";
import cloudsIcon from "../../images/Clouds_icon.png";
import rainIcon from "../../images/Rain_icon.png";
import sunIcon from "../../images/Sun_icon.png";

const breakpoints = {
  height: "825px"
};

export const LoginInput = styled.input`
  background: none;
  border: none;
  border-bottom: 2px solid white;
  font-size: 1.5em;
  color: white;

  &:focus {
    outline: none;
  }

  &::placeholder {
    color: white;
  }
`;

export const LoginText = styled.p`
  font-size: 2em;
  color: white;
  margin: auto;
  margin-bottom: 1vh;
`;

export const LoginSpan = styled.span`
  font-size: 1em;
  color: yellow;
  text-decoration: none;
`;

export const LoginButton = styled.button`
  border: none;
  padding: 10px 60px;
  background-color: yellow;
  color: black;
  border-radius: 20px;
  cursor: pointer;
  width: 50%;

  &:hover {
    background-color: #ffffed;
  }
`;

export const LoginForm = styled.form`
  width: 50vw;
  height: 30vh;
  margin: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const SignupForm = styled.form`
  width: 50vw;
  height: 30vh;
  margin: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const LoginFields = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const DashboardHeader = styled.h1`
  font-size: 3em;
  color: white;
`;

export const DashboardContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const WidgetContainer = styled.div`
  @media screen and (max-width: 720px) {
  }
  @media screen and (min-width: 720px) {
    display: grid;
    width: 90vw;
    height: 70vh;
    grid-template-columns: repeat(3, 30vw);
    grid-template-rows: repeat(2, 35vh);
  }
`;

export const WidgetElement = styled.div`
  display: grid;
  grid-template-rows: 25% 75%;
  width: 90%;
  height: 90%;
  margin-top: 10px;
  margin-bottom: 10px;
  justify-self: center;
  align-self: center;
  background-image: url("${widgetContainer}");
  background-size: 100% 100%;
  background-repeat: no-repeat;
`;

export const WidgetBody = styled.div`
  width: 80%;
  height: 80%;
  margin: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const WidgetTitle = styled.h2`
  text-align: center;
  @media screen and (min-height: ${breakpoints.height}) {
    font-size: 2.25em;
  }
  @media screen and (max-height: ${breakpoints.height}) {
    font-size: 1.5em;
  }
`;

export const FlexRow = styled.div`
  display: flex;
  width: 90%;
  margin-bottom: 10px;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const WidgetBodyTitle = styled.h3`
  text-align: center;
  font-weight: 700;
  @media screen and (min-height: ${breakpoints.height}) {
    font-size: 1.07em;
  }
  @media screen and (max-height: ${breakpoints.height}) {
    font-size: 0.75em;
  }
`;

export const WidgetBodySubtitle = styled.h3`
  text-align: center;
  font-weight: 500;
  @media screen and (min-height: ${breakpoints.height}) {
    font-size: 1.07em;
  }
  @media screen and (max-height: ${breakpoints.height}) {
    font-size: 0.75em;
  }
`;

export const WeatherTemperature = styled.h4`
  text-align: center;
  margin: 0;
  @media screen and (min-height: ${breakpoints.height}) {
    font-size: 2em;
  }
  @media screen and (max-height: ${breakpoints.height}) {
    font-size: 1.5em;
  }
`;

export const WeatherLocation = styled.h4`
  text-align: center;
  margin: 0;
  @media screen and (min-height: ${breakpoints.height}) {
    font-size: 2em;
  }
  @media screen and (max-height: ${breakpoints.height}) {
    font-size: 1.5em;
  }
`;

export const WeatherIconContainer = styled.div`
  display: block;
  width: 30%;
`;

export const WeatherIconImage = styled.img`
  width: 100%;
`;

export const WeatherIcon = ({ icon }) => {
  return (
    <WeatherIconContainer>
      {icon === "Clouds" ? (
        <WeatherIconImage src={cloudsIcon} alt="Cloudy" />
      ) : (
        <>
          {icon === "Rainy" ? (
            <WeatherIconImage src={rainIcon} alt="Rainy" />
          ) : (
            <WeatherIconImage src={sunIcon} alt="Sunny" />
          )}
        </>
      )}
    </WeatherIconContainer>
  );
};
