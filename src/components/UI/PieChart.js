import React, { useState, useEffect } from "react";
import styled from "styled-components";

import { generatePieChartPaths } from "../../utils/calculations";

/*
    Generates a SVG pie chart, taking a 'total' value and object of 'elements' with counts and using trigonometry to draw the path
*/

const PieChart = ({ diameter, total, elements }) => {
  const [pieChartPaths, setPieChartPaths] = useState(null);
  useEffect(() => {
    setPieChartPaths(generatePieChartPaths(total, elements));
    // eslint-disable-next-line
  }, []);

  return (
    <Pie diameter={diameter} viewBox="-1 -1 2 2">
      {pieChartPaths ? (
        <>
          {console.log(pieChartPaths)}
          {pieChartPaths.map(path => (
            <path d={path.path} fill={path.fill}>
              <title>{path.title}</title>
            </path>
          ))}
        </>
      ) : null}
    </Pie>
  );
};

// Styled components

const Pie = styled.svg`
  transform: rotate(270deg);
  width: 25vw;
  height: 25vw;
`;

export default PieChart;
