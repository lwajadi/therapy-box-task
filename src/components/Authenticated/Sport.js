import React, { useState } from "react";
import styled from "styled-components";

/*
    Sport page
*/

import {
  LoginButton,
  LoginFields,
  LoginInput,
  SignupForm
} from "../UI/Elements";

const Sport = ({ sportState, selectTeam }) => {
  const [team, setTeam] = useState(null);

  const handleTeamChange = event => {
    setTeam(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (team) selectTeam(team);
  };

  return (
    <Div>
      <H1>Sport</H1>
      <SignupForm>
        <LoginFields>
          <LoginInput
            value={team}
            onChange={handleTeamChange}
            placeholder="Enter team name"
          />
        </LoginFields>
        <LoginButton onClick={handleSubmit}>Search</LoginButton>
      </SignupForm>
      <TeamList>
        {sportState.wonAgainst ? (
          <>
            {sportState.wonAgainst.map(team => (
              <BeatenTeam>{team}</BeatenTeam>
            ))}
          </>
        ) : null}
      </TeamList>
    </Div>
  );
};

const H1 = styled.h1`
  font-size: 3.5em;
  color: white;
  margin-left: 10px;
  text-align: left;
`;

const BeatenTeam = styled.p`
  font-size: 1em;
  color: white;
  margin-top: 5px;
  margin-bottom: 5px;
`;

const Div = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const TeamList = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 50%;
  justify-content: center;
  align-items: center;
`;

export default Sport;
