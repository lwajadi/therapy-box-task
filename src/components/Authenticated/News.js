import React from "react";
import styled from "styled-components";

/*
    **TODO**

    News page
*/

const News = ({ newsState }) => {
  return (
    <Div>
      <H1>News</H1>
      <H2>{newsState.headline}</H2>
      <H3>{newsState.text}</H3>
    </Div>
  );
};

const H1 = styled.h1`
  font-size: 3.5em;
  color: white;
  margin-left: 10px;
  text-align: left;
`;

const H2 = styled.h2`
  font-size: 2.5em;
  width: 50%;
  color: white;
  margin-left: 10px;
  text-align: center;
`;
const H3 = styled.h3`
  font-size: 1.5em;
  color: white;
  width: 50%;
  margin-left: 10px;
  text-align: center;
`;

const Div = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

export default News;
