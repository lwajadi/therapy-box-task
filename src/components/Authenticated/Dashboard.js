import React, { useContext } from "react";
import { Link } from "react-router-dom";

import {
  DashboardHeader,
  DashboardContainer,
  WidgetContainer,
  WidgetElement,
  WidgetBody,
  WidgetTitle,
  WidgetBodyTitle,
  WidgetBodySubtitle,
  WeatherTemperature,
  WeatherLocation,
  WeatherIcon,
  FlexRow
} from "../UI/Elements";

import PieChart from "../UI/PieChart";

import { AuthContext } from "../../context/AuthProvider";

/*
    **TODO**

    Dashboard page
*/

const Widget = ({ url, title, children }) => {
  return (
    <WidgetElement>
      {url ? (
        <Link to={url} style={{ textDecoration: "none" }}>
          <WidgetTitle>{title}</WidgetTitle>
        </Link>
      ) : (
        <WidgetTitle>{title}</WidgetTitle>
      )}
      <WidgetBody>{children}</WidgetBody>
    </WidgetElement>
  );
};

const Dashboard = ({
  weatherState,
  newsState,
  sportState,
  photosState,
  tasksState,
  clothesState
}) => {
  const { username } = useContext(AuthContext);
  return (
    <DashboardContainer>
      <DashboardHeader>
        Good day {username ? username : "Swapnil"}
      </DashboardHeader>
      <WidgetContainer>
        <Widget title={"Weather"}>
          <FlexRow>
            <WeatherIcon icon={weatherState.icon} />
            <WeatherTemperature>
              {weatherState.temperature + "°C"}
            </WeatherTemperature>
          </FlexRow>
          <WeatherLocation>{weatherState.location}</WeatherLocation>
        </Widget>
        <Widget url={"/news"} title={"News"}>
          <WidgetBodyTitle>{newsState.headline}</WidgetBodyTitle>
          <WidgetBodySubtitle>{newsState.text}</WidgetBodySubtitle>
        </Widget>
        <Widget url={"/sport"} title={"Sport"}>
          {console.log(sportState)}
          {sportState ? (
            <>
              {console.log(sportState)}
              <WidgetBodyTitle>
                {sportState.selectedTeam
                  ? sportState.selectedTeam
                  : "No team selected"}
              </WidgetBodyTitle>
              <WidgetBodySubtitle>
                {sportState.selectedTeam
                  ? `${sportState.selectedTeam} are the best in the world!`
                  : `Don't want to hedge your bets, huh?`}
              </WidgetBodySubtitle>
            </>
          ) : null}
        </Widget>
        <Widget url={"/photos"} title={"Photos"}>
          <p>TODO</p>
        </Widget>
        <Widget url={"/tasks"} title={"Tasks"}>
          <p>TODO</p>
        </Widget>
        <Widget title={"Clothes"}>
          {clothesState.items ? (
            <PieChart
              diameter={200}
              total={clothesState.total}
              elements={clothesState.items}
            />
          ) : null}
        </Widget>
      </WidgetContainer>
    </DashboardContainer>
  );
};

// Styled Components

export default Dashboard;
