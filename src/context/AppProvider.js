import React from "react";

import AuthProvider from "./AuthProvider";

/*
    **TODO**
    
    This component is home for all context providers that need to be consumed app-wide
*/

const AppProvider = ({ children }) => {
  return <AuthProvider>{children}</AuthProvider>;
};

export default AppProvider;
