import React, { useState, createContext } from "react";
import axios from "axios";

export const AuthContext = createContext();

/*
    All authorization logic stored here. Allows for additional security methods to be added if required in the future
*/

const AuthProvider = ({ children }) => {
  const [userId, setUserId] = useState(null);
  const [usernameState, setUsernameState] = useState(null);

  // NOTE: Alternative to localStorage would be required for full-fledged project with security concerns.
  const setToken = token => {
    localStorage.setItem("token", token);
  };

  const getToken = () => {
    return localStorage.getItem("token") || null;
  };

  const setUser = id => {
    setUserId(id);
  };

  const setUsername = username => {
    setUsernameState(username);
  };

  return (
    <AuthContext.Provider
      value={{
        userId: userId,
        username: usernameState,
        setToken: setToken,
        getToken: getToken,
        setUser: setUser,
        setUsername: setUsername
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
